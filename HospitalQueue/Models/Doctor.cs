﻿namespace HospitalQueue.Models
{
    public class Doctor
    {
        public Doctor(int id)
        {
            ID = id;
            IsBusy = false;
        }

        public int ID { get; set; }
        public bool IsBusy { get; set; }
    }
}