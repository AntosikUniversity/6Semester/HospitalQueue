﻿using System;

namespace HospitalQueue.Models
{
    public class Visitor
    {
        public bool IsSick { get; set; }
        public DateTime ArriveTime;
        public DateTime ExamineTime;

        public Visitor()
        {
            IsSick = false;
            ArriveTime = DateTime.Now;
        }
    }
}