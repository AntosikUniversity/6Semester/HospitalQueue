﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using HospitalQueue.Helpers;
using HospitalQueue.Models;

namespace HospitalQueue.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public static Random Random = new Random();

        private int _busyDoctors;
        private ConcurrentBag<Doctor> _doctors;

        private int _doctorsCount;
        private int _examineTime;
        private int _freeDoctors;
        private int _infectionTime;
        private string _log;
        private int _peopleChance;
        private int _roomSize;
        private int _sickChance;
        private int _sickVisitorsInQueue;

        private ConcurrentQueue<Visitor> _visitors;
        private int _visitorsCount;
        private int _visitorsInQueue;
        private int _visitorsInRoom;

        public MainViewModel()
        {
            Visitors = new ConcurrentQueue<Visitor>();
            Doctors = new ConcurrentBag<Doctor>();

            DoctorsCount = 4;
            RoomSize = 20;
            ExamineTime = 15;
            SickChance = 15;
            PeopleChance = 75;
            Log = string.Empty;
            InfectionTime = 20;

            VisitorsCount = 0;
            VisitorsInRoom = 0;
            VisitorsInQueue = 0;
            SickVisitorsInQueue = 0;

            RunSimulation = new Command(Simulate);
        }

        public int DoctorsCount
        {
            get => _doctorsCount;
            set => Change(ref _doctorsCount, value);
        }

        public int RoomSize
        {
            get => _roomSize;
            set => Change(ref _roomSize, value);
        }

        public int ExamineTime
        {
            get => _examineTime;
            set => Change(ref _examineTime, value);
        }

        public int PeopleChance
        {
            get => _peopleChance;
            set => Change(ref _peopleChance, value);
        }

        public int SickChance
        {
            get => _sickChance;
            set => Change(ref _sickChance, value);
        }

        public string Log
        {
            get => _log;
            set => Change(ref _log, value);
        }

        public ICommand RunSimulation { get; }

        public ConcurrentQueue<Visitor> Visitors
        {
            get => _visitors;
            set => Change(ref _visitors, value);
        }

        public ConcurrentBag<Doctor> Doctors
        {
            get => _doctors;
            set => Change(ref _doctors, value);
        }

        public int BusyDoctors
        {
            get => _busyDoctors;
            set => Change(ref _busyDoctors, value);
        }

        public int FreeDoctors
        {
            get => _freeDoctors;
            set => Change(ref _freeDoctors, value);
        }

        public int VisitorsInRoom
        {
            get => _visitorsInRoom;
            set => Change(ref _visitorsInRoom, value);
        }

        public int VisitorsInQueue
        {
            get => _visitorsInQueue;
            set => Change(ref _visitorsInQueue, value);
        }

        public int SickVisitorsInQueue
        {
            get => _sickVisitorsInQueue;
            set => Change(ref _sickVisitorsInQueue, value);
        }

        public int VisitorsCount
        {
            get => _visitorsCount;
            set => Change(ref _visitorsCount, value);
        }

        public int InfectionTime
        {
            get => _infectionTime;
            set => Change(ref _infectionTime, value);
        }

        public void Simulate()
        {
            Log = string.Empty;

            Visitors = new ConcurrentQueue<Visitor>();
            Doctors = new ConcurrentBag<Doctor>();

            for (var i = 0; i < DoctorsCount; i++)
                Doctors.Add(new Doctor(i + 1));

            RecalculateProperties();

            RunVisitors();
            RunDoctors();
        }

        private void RecalculateProperties()
        {
            BusyDoctors = Doctors.Count(doctor => doctor.IsBusy);
            FreeDoctors = DoctorsCount - BusyDoctors;

            VisitorsCount = Visitors.Count;
            VisitorsInRoom = VisitorsCount > RoomSize ? RoomSize : VisitorsCount;

            if (VisitorsInRoom != RoomSize)
            {
                VisitorsInQueue = 0;
                SickVisitorsInQueue = 0;
                return;
            }

            VisitorsInQueue = VisitorsCount - VisitorsInRoom;
            SickVisitorsInQueue = Visitors.Skip(VisitorsInRoom).Count(visitor => visitor.IsSick);

            if (SickVisitorsInQueue == 0) return;

            var sickVisitorsInQueue = Visitors.Skip(VisitorsInRoom).Where(visitor => visitor.IsSick);
            foreach (var sickVisitor in sickVisitorsInQueue)
            {
                var notSickVisitorsInQueue = Visitors.Skip(VisitorsInRoom).Where(visitor => !visitor.IsSick).ToList();

                if (notSickVisitorsInQueue.Count == 0 || DateTime.Now.Subtract(sickVisitor.ArriveTime) <
                    TimeSpan.FromSeconds(InfectionTime)) continue;

                var needMod = notSickVisitorsInQueue
                    .Where(visitor => visitor.ArriveTime < sickVisitor.ArriveTime).ToList();

                if (needMod.Count > 0)
                {
                    for (int i = 0, len = needMod.Count; i < len; i++) needMod[i].IsSick = true;

                    Log += $"${DateTime.Now}: Стоя в очереди заразилось {needMod.Count} посетителей! \n";
                }
            }

            SickVisitorsInQueue = Visitors.Skip(VisitorsInRoom).Count(visitor => visitor.IsSick);
        }

        #region Visitors

        private void RunVisitors()
        {
            Task.Run(() => RunningVisitor());
        }

        private async void RunningVisitor()
        {
            while (true)
            {
                if (IsVisitorCame())
                {
                    var sick = IsVisitorSick();
                    var visitor = new Visitor {IsSick = IsVisitorSick()};

                    var visitorsCount = Visitors.Count;
                    var roomStr = visitorsCount > RoomSize ? "в очереди" : "в приемной";
                    var sickStr = sick ? "Больной" : "Здоровый";

                    Log += $"{visitor.ArriveTime}: {sickStr} пациент ждет {roomStr}. \n";

                    Visitors.Enqueue(visitor);
                    RecalculateProperties();
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private bool IsVisitorCame()
        {
            return Random.Next(0, 100) < _peopleChance;
        }

        private bool IsVisitorSick()
        {
            return Random.Next(0, 100) < _sickChance;
        }

        #endregion

        #region Doctors

        private void RunDoctors()
        {
            Task.Run(() => Parallel.ForEach(Doctors, ExamineVisitor));
        }

        private async void ExamineVisitor(Doctor doctor)
        {
            while (true)
            {
                if (Visitors.Count == 0)
                {
                    continue;
                }

                if (Visitors.TryDequeue(out var visitor))
                {
                    visitor.ArriveTime = DateTime.Now;
                    Log += $"{visitor.ArriveTime}: Доктор {doctor.ID} принял пациента. \n";
                    doctor.IsBusy = true;

                    RecalculateProperties();

                    await Task.Delay(TimeSpan.FromSeconds(ExamineTime));

                    doctor.IsBusy = false;
                    Log += $"${DateTime.Now}: Доктор {doctor.ID} освободился. \n";

                    RecalculateProperties();
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        #endregion
    }
}